import { configConst } from './const'
import axios from 'axios'
import Cookies from 'js-cookie'

export default class UserController {
  static isLoggedIn () {
    const cookieBearer = Cookies.get('Bearer')
    return !(cookieBearer === undefined || cookieBearer === '')
  }

  static getUserId () {
    return Cookies.get('userId')
  }

  static logout () {
    Cookies.remove('Bearer', { path: '/' })
    Cookies.remove('userId', { path: '/' })
  }

  static login (email, password) {
    const data = {
      username: email,
      password: password
    }
    return axios({
      method: 'POST',
      url: configConst.serverUrlAuth + 'auth',
      withCredentials: true,
      headers: {},
      data: data
    })
      .then(r => {
        if (r.status === 200) {
          return 1
        } else {
          return -1
        }
      })
      .catch(r => {
        if (r.status === 401) {
          return 0
        } else {
          return -1
        }
      })
  }

  static createNewUser (email, password, name) {
    const data = {
      email: email,
      name: name,
      password: password
    }
    console.log(data)
    return axios({
      method: 'POST',
      url: configConst.serverUrl + 'user-service/register',
      headers: {},
      data: data
    })
      .then(r => {
        if (r.status === 201) {
          return 1
        } else {
          return -1
        }
      })
      .catch(r => {
        if (r.response.status === 409) {
          return 0
        } else {
          return -1
        }
      })
  }

  static changePassword (userId, actualPassword, newPassword) {
    console.log(userId, actualPassword, newPassword)
    return true
  }
}

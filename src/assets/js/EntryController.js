export default class EntryController {
  static getEntry (startDate, endDate) {
    return [
      {
        expenseId: 1,
        idUser: 1,
        account: {
          accountId: 1,
          name: 'testowe',
          idUser: 1
        },
        amount: -1500,
        date: '2020-01-02 20:58',
        note: 'dnvijvbsaiokbdfiujsbavujf',
        subcategory: {
          subcategoryId: 1,
          name: 'Podkategoria Testowa',
          category: {
            categoryId: 1,
            name: 'Kategoria testowa',
            idUser: 1,
            color: {
              colorId: 1,
              color: '#BFACBA'
            }
          },
          is_income: false
        },
        type: 'EXPENSE'
      },
      {
        expenseId: 2,
        idUser: 1,
        account: {
          accountId: 1,
          name: 'testowe',
          idUser: 1
        },
        amount: -15000,
        date: '2020-01-01 20:58',
        note: 'dnvijvbsaiokbdfiujsbavujf',
        subcategory: {
          subcategoryId: 1,
          name: 'Podkategoria Testowa',
          category: {
            categoryId: 1,
            name: 'Kategoria testowa',
            idUser: 1,
            color: {
              colorId: 1,
              color: '#BFACBA'
            }
          },
          is_income: false
        },
        type: 'EXPENSE'
      },
      {
        expenseId: 3,
        idUser: 1,
        account: {
          accountId: 1,
          name: 'testowe',
          idUser: 1
        },
        amount: -150000,
        date: '2020-01-01 20:58',
        note: 'dnvijvbsaiokbdfiujsbavujf',
        subcategory: {
          subcategoryId: 1,
          name: 'Podkategoria Testowa',
          category: {
            categoryId: 1,
            name: 'Kategoria testowa',
            idUser: 1,
            color: {
              colorId: 1,
              color: '#BFACBA'
            }
          },
          is_income: false
        },
        type: 'EXPENSE'
      },
      {
        expenseId: 4,
        idUser: 1,
        account: {
          accountId: 1,
          name: 'testowe',
          idUser: 1
        },
        amount: -1500000,
        date: '2020-01-01 20:58',
        note: 'dnvijvbsaiokbdfiujsbavujf',
        subcategory: {
          subcategoryId: 1,
          name: 'Podkategoria Testowa',
          category: {
            categoryId: 1,
            name: 'Kategoria testowa',
            idUser: 1,
            color: {
              colorId: 1,
              color: '#BFACBA'
            }
          },
          is_income: false
        },
        type: 'EXPENSE'
      },
      {
        expenseId: 5,
        idUser: 1,
        account: {
          accountId: 1,
          name: 'testowe',
          idUser: 1
        },
        amount: -15000000,
        date: '2020-01-01 20:58',
        note: 'dnvijvbsaiokbdfiujsbavujf',
        subcategory: {
          subcategoryId: 1,
          name: 'Podkategoria Testowa',
          category: {
            categoryId: 1,
            name: 'Kategoria testowa',
            idUser: 1,
            color: {
              colorId: 1,
              color: '#BFACBA'
            }
          },
          is_income: false
        },
        type: 'EXPENSE'
      },
      {
        expenseId: 6,
        idUser: 1,
        account: {
          accountId: 1,
          name: 'testowe',
          idUser: 1
        },
        amount: -150000000,
        date: '2020-01-01 20:58',
        note: 'dnvijvbsaiokbdfiujsbavujf',
        subcategory: {
          subcategoryId: 1,
          name: 'Podkategoria Testowa',
          category: {
            categoryId: 1,
            name: 'Kategoria testowa',
            idUser: 1,
            color: {
              colorId: 1,
              color: '#BFACBA'
            }
          },
          is_income: false
        },
        type: 'EXPENSE'
      },
      {
        expenseId: 7,
        idUser: 1,
        account: {
          accountId: 1,
          name: 'testowe',
          idUser: 1
        },
        amount: -1500000000,
        date: '2020-01-01 20:58',
        note: 'dnvijvbsaiokbdfiujsbavujf',
        subcategory: {
          subcategoryId: 1,
          name: 'Podkategoria Testowa',
          category: {
            categoryId: 1,
            name: 'Kategoria testowa',
            idUser: 1,
            color: {
              colorId: 1,
              color: '#BFACBA'
            }
          },
          is_income: false
        },
        type: 'EXPENSE'
      },
      {
        expenseId: 8,
        idUser: 1,
        account: {
          accountId: 1,
          name: 'testowe',
          idUser: 1
        },
        amount: -150000000000,
        date: '2020-01-01 20:58',
        note: 'dnvijvbsaiokbdfiujsbavujf',
        subcategory: {
          subcategoryId: 2,
          name: 'Podkategoria Testowa2',
          category: {
            categoryId: 2,
            name: 'Kategoria testowa2',
            idUser: 1,
            color: {
              colorId: 1,
              color: '#BFACBA'
            }
          },
          is_income: false
        },
        type: 'EXPENSE'
      }
    ]
  }

  static getLast (amount) {

  }

  static addNewEntryIncomeOutcome (accountId, amount, subcategoryId, date, note, type, userId) {
    const obj = {
      account: accountId,
      idUser: userId,
      amount: amount,
      date: date,
      note: note,
      subcategoryId: subcategoryId,
      type: type
    }
    console.log(obj)
    return true
  }

  static addNewEntryTransfer (sourceAccountId, destinationAccountId, amount, date, note, type, userId) {
    const obj = {
      sourceAccountId: sourceAccountId,
      destinationAccountId: destinationAccountId,
      idUser: userId,
      amount: amount,
      date: date,
      note: note,
      type: type
    }
    console.log(obj)
    return true
  }
}

// let byłoByCudownie = {
//   expenseId: 8,
//   idUser: 1,
//   accountId: 1,
//   name: 'testowe',
//   amount: -150000000000,
//   date: '2020-01-01 20:58',
//   note: 'dnvijvbsaiokbdfiujsbavujf',
//   subcategoryId: 2,
//   subcategoryName: 'Podkategoria Testowa2',
//   categoryId: 2,
//   categoryName: 'Kategoria testowa2',
//   colorId: 1,
//   color: '#BFACBA',
//   is_income: false,
//   type: 'EXPENSE'
// }
